import React from 'react'
import LocationOnOutlinedIcon from '@material-ui/icons/LocationOnOutlined';
import {
  CardsTitle,
} from "./cards"

  

 const CardsHeader = ({item}) => {

  return (
    <div style={{
        textAlign: "right",
        marginBottom: "5px"
      }}>
      <CardsTitle dense={true} font={16}>{item.title}</CardsTitle>
      <CardsTitle  display={"inline"} font={10} color={"#464141f0"}>{item.text.slice(0, 40)}</CardsTitle> 
      <LocationOnOutlinedIcon style={{
        color: "#bbb",
        display: "inline"
      }} />
      </div>
  )
}
export default CardsHeader
import styled from 'styled-components'


const Cards = styled.div`
width: 25rem;
    height: 20rem;
    margin: 10px;
    position: relative;
    display: flex;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-color: #fff;
    background-clip: border-box;
    border: 1px solid rgba(0,0,0,.125);
    border-radius: .25rem;
 `

 const CardsBody = styled.div`
 flex: 1 1 auto;
 min-height: 1px;
 padding: 0.5rem 1.25rem;
 `

 const CardsTitle = styled.div`
 margin-bottom: ${(props) => props.dense? "0"  : ".75rem"};
 color: ${(props) => props.color || "#000"};
 font-size: ${(props) => props.font+ "px"};
 text-align: right;
 display: ${(props) => props.display || "block"};

 `

 
 const CardsImage = styled.img`
 border-top-left-radius: calc(.25rem - 1px);
    border-top-right-radius: calc(.25rem - 1px);
    flex-shrink: 0;
    width: 70%;
    height: 50%;
    margin-bottom: 10px;
 `


export {
    Cards,
    CardsBody,
    CardsTitle,
    CardsImage
}
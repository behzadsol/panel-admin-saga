import axios from "axios";
import { apiEndpoint } from "./http.config.json";

export default ({
  url = "",
  method = "get",
  headers = {},
  params = {},
  data = {}
} = {}) => {
  // config defaults for instance
  const instance = axios.create({
    headers: {
      'Content-Type': 'application/json',
      ...headers
    },
    data,
    baseURL: apiEndpoint,
    timeout: 10000,
    maxContentLength: Math.pow(2, 10) * 50,
    withCredentials: false,
    maxRedirects: 5,
    validateStatus: status => status >= 200 && status < 600
  });

  // interceptors
  instance.interceptors.request.use(
    config => {
      return config;
    },
    error => {
      return Promise.reject(error);
    }
  );
  instance.interceptors.response.use(
    response => {
      return response;
    },
    error => {
      return Promise.reject(error);
    }
  );

  // return instance
  return instance({ method, url, params, data });
};

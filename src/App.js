import React from "react";
import "./App.css";
import AppProvider from "./redux/store";
import { Route, BrowserRouter as Router, Switch } from "react-router-dom";
import Home from "./Components/home";
import Posts from "./Components/posts-user";
import Login from "./Components/login.jsx";

function App() {
  return (
    <AppProvider>
      <div className="App">
        <div className="App-header">
          <Router>
            <Switch>
            <Route path="/" exact={true} component={Login} />
            <Route path="/home"  component={Home} />
              <Route path="/posts/:id" component={Posts} />
            </Switch>
          </Router>
        </div>
      </div>
    </AppProvider>
  );
}

export default App;

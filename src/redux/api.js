import http from "../api";


export const fetchData = async (pageNumber) => {
    try {
       const res = await http({
            url: "/post/search",
            method: "post",
            headers: {
              t: "iKHATs09al3iNZnRYbYgN3Qrm/z1VHpfT3P7gbgxxW0="
            },
            data: {
              lm: 9,
              pg: pageNumber,
              tp: 1
            }
          })
          return res.data && res.data.result;
    } catch (e) {
      console.log(e);
    }
  };
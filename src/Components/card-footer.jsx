import React from 'react'
import ShareOutlinedIcon from '@material-ui/icons/ShareOutlined';
import PhoneOutlinedIcon from '@material-ui/icons/PhoneOutlined';

  

 const CardsFooter = () => {

  return (
    <div style={{
        display: "flex",
        justifyContent: "space-around"
      }}>
      <ShareOutlinedIcon style={{
        color: "#000"
      }} />
      <PhoneOutlinedIcon  style={{
        color: "#000"
      }}/>
      </div>
  )
}
export default CardsFooter
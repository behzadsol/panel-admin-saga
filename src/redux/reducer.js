import { SET_ITEMS, FAIL_ITEMS, SET_ACTIVE , FETCH_DATA } from "./types";

const reducer = (state = {}, action) => {

  switch (action.type) {
    case SET_ITEMS:
      return { ...state, listItems: action.payload , loading: false };

    case FAIL_ITEMS:
      return { ...state, listItems: [] , loading: false };

      
    case FETCH_DATA:
        return { ...state, listItems: [] , loading: true };

    case SET_ACTIVE:
      return { ...state, active: action.payload };

    default:
      return state;
  }
};

export default reducer;

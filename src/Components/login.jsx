import React from 'react'
import { useHistory } from "react-router-dom";
import styled from 'styled-components'



const Buttons = styled.button`
display: block;
    width: 80%;
    padding: .5rem 1rem;
    font-size: 1.25rem;
    line-height: 1.5;
    border-radius: .3rem;
    color: #fff !important;
    background-color: #28a745 !important;
    border-color: #007bff;
    display: inline-block;
    font-weight: 400;
    border: 1px solid transparent;
    padding: .375rem .75rem;
    font-size: 1rem;
    line-height: 1.5;
    border-radius: .25rem;

`


const Login = () => {
    const history = useHistory()

const handleClick = () => {
    history.push("/home")
}

  return (
   <>
  <Buttons onClick={handleClick} >
    برای ورود کلیک نمایید
  </Buttons>
   </>
  )
}
export default Login
import { SET_ITEMS, FAIL_ITEMS, SET_ACTIVE , FETCH_DATA } from "./types";

export const setItems = items => ({
  type: SET_ITEMS,
  payload: items
});

export const failedItems = () => ({
  type: FAIL_ITEMS
});

export const setActive = active => ({
  type: SET_ACTIVE,
  payload: active
});

export const gePosts = () => ({
  type: FETCH_DATA,
});




// redux thunk ...

// export const fetchItems = (page = 1) => {
//   return async dispatch => {
//     await http({
//       url: "/post/search",
//       method: "post",
//       headers: {
//         t: "iKHATs09al3iNZnRYbYgN3Qrm/z1VHpfT3P7gbgxxW0="
//       },
//       data: {
//         lm: 9,
//         pg: page,
//         tp: 1
//       }
//     })
//       .then(resp => dispatch(setItems(resp.data)))
//       .catch(() => dispatch(failedItems()));
//   };
// };

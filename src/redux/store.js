import React from "react";
import { Provider } from "react-redux";
import { createStore , applyMiddleware , compose } from "redux";
import reducer from "./reducer";
// import thunk from "redux-thunk";
import createSagaMiddleware from 'redux-saga'
import mySaga from "./saga";


function AppProvider(props){


 const initialState = {
    listItems: [],
    active: 1,
    loading: false
  };



  // const reducerCombine = combineReducers({
  //   reducer,
  // });
  const sagaMiddleware = createSagaMiddleware()


  const store = createStore(reducer, initialState , compose(
    applyMiddleware(sagaMiddleware),
  ));

  sagaMiddleware.run(mySaga);


    return <Provider store={store}>{props.children}</Provider>;
}

export default AppProvider;

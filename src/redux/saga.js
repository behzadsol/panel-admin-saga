import { call, put, takeLatest , select  } from "redux-saga/effects";
import { FETCH_DATA } from './types';
import {  fetchData } from "./api";
import {  setItems , failedItems } from "./action";

function* getApiData() {
  try {
    const getActive = (state) => state.active

    let active = yield select(getActive);

    const data = yield call(() => fetchData(active));
    yield put(setItems(data.data));
  } catch (e) {
    yield put(failedItems());

  }
}


export default function* mySaga() {
  yield takeLatest(FETCH_DATA, getApiData);

}
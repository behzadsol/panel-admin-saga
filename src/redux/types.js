export const SET_ITEMS = 'SET_ITEMS';
export const FAIL_ITEMS = 'FAIL_ITEMS';
export const SET_ACTIVE = 'SET_ACTIVE';
export const FETCH_DATA = 'FETCH_DATA';

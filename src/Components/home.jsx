import React, { useEffect } from "react";
import styled from "styled-components";
import { gePosts } from "../redux/action";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import ClipLoader from "react-spinners/ClipLoader";
import { Cards, CardsBody, CardsImage } from "./cards";
import CardHeader from "./card-header";
import CardsFooter from "./card-footer";
import Paginations from "./pagination"

const StyledContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center
`;




const Home = () => {
  const dispatch = useDispatch();
  const items = useSelector(state => state.listItems);
  const active = useSelector(state => state.active);
  const loading = useSelector(state => state.loading);

  useEffect(() => {
    dispatch(gePosts(active));
  }, [active]);


  return (
    loading ? (
        <ClipLoader loading={true} size={150} />
      ) : !items.length ? <span>
        داده‌ای یافت نشد
      </span> : (
      <>

       <Paginations />
    <StyledContainer>
        <>
          {items.map((itm, index) => (
            <Cards key={index}>
              <CardsBody>
                <CardHeader item={itm} />
                <Link
                  to={{
                    pathname: "/posts/" + itm.idc,
                    state: { content: itm.text }
                  }}
                >
                  <CardsImage src="https://picsum.photos/id/0/200/300" />
                </Link>
                <CardsFooter />
              </CardsBody>
            </Cards>
          ))}
        </>
    </StyledContainer>
    </>
  ))
};
export default Home;

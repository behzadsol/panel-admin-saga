

import React from 'react'
import { Pagination } from "react-bootstrap";
import styled from "styled-components";
import { setActive } from "../redux/action";
import { useDispatch, useSelector } from "react-redux";

  
const Paginations = styled.ul`
margin-top: 5%;
display: flex;
padding-left: 0;
list-style: none;
border-radius: .25rem;
margin-bottom: 1rem;
margin-block-start: 1em;
    margin-block-end: 1em;
    margin-inline-start: 0px;
    margin-inline-end: 0px;
    padding-inline-start: 40px;
`;

 const PaginationComponent = ({item}) => {
     const pageItems = [];
     const dispatch = useDispatch();
     const active = useSelector(state => state.active);


    for (let number = 1; number <= 19; number++) {
        pageItems.push(
          <>
          <Pagination.Item
            key={number}
            active={number === active}
            onClick={() => dispatch(setActive(number))}
          >
            {number}
          </Pagination.Item>
            </>
        );
      }
  return (
    <Paginations >
    {pageItems}
  </Paginations>
  )
}
export default PaginationComponent